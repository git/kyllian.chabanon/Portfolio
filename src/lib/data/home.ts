import { Platform } from '$lib/types';
import { getSkills } from './skills';

export const title = 'Accueil';

export const name = 'Kyllian';

export const lastName = 'Chabanon';

export const description =
	'Salut ! Je m’appelle Kyllian Chabanon, et je suis étudiant en deuxième année BUT Informatique à Clermont-Ferrand. Ayant toujours été attiré par l’informatique en général, et plus particulièrement par la programmation, c’est tout naturellement que je me suis orienté vers cette formation.';

export const links: Array<{ platform: Platform; link: string }> = [
	{
		platform: Platform.Linkedin,
		link: 'https://www.linkedin.com/in/kyllian-chabanon'
	},
	{
		platform: Platform.Email,
		link: 'kyllianchabanon@gmail.com'
	},
];

export const skills = getSkills('js', 'css', 'html', 'flutter', 'dart', 'java', 'csharp', 'python');
