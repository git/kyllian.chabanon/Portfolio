export const items = [
	{ title: 'Compétences', to: '/skills', icon: 'i-carbon-software-resource-cluster' },
	{ title: 'Projets', to: '/projects', icon: 'i-carbon-cube' },
	{ title: 'CV', to: '/resume', icon: 'i-carbon-result' }
] as const;
