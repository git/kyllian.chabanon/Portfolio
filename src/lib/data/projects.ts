import Assets from './assets';
import { getSkills } from './skills';
import type { Project } from '../types';

export const items: Array<Project> = [
	{
		slug: 'mimir',
		color: '#8d05c7',
		description:
			'Mimir est une application web et mobile de gestion de tuteurs faite dans le cadre d\'une SAÉ pour mon IUT. C\'est un travail de groupe que j\'ai dû faire avec l\'aide de deux de mes camarades. Nous avons dû réaliser un développement d\'application (C1) ainsi que son optimisation (C2). Afin de gérer les données des utilisateurs, il a été nécessaire de gérer des bases de données (C4) en SQL ainsi qu\'une API REST en C#. Nous avons aussi dû gérer des systèmes informatiques (C3) afin de déployer les applications en Docker. Finalement, comme dans tout projet à plusieurs, il a fallu apprendre à gérer un projet (C5) et collaborer afin que tout se déroule bien (C6).',
		shortDescription:
			'Mimir est une application de gestion de tuteurs faite dans le cadre d\'une SAÉ pour mon IUT.',
		links: [],
		logo: Assets.Mimir,
		name: 'Mimir',
		period: {
			from: new Date(2023, 9, 1)
		},
		skills: getSkills('js', 'php', 'html', 'css', 'csharp', 'kotlin', 'docker'),
		type: 'Application de gestion de tuteurs',
		screenshots: [
			{
				label: 'Page d\'ajout d\'une séance',
				src: 'https://cdn.discordapp.com/attachments/901792204224352266/1226715001021267978/ajout-seance.png?ex=6625c65f&is=6613515f&hm=93bbb75c7ef32f7fe961dee0ff5c195245e681f3b517cfdbbf0af6976ba53985&'
			},
			{
				label: 'Page de gestion des comptes',
				src: 'https://cdn.discordapp.com/attachments/901792204224352266/1226714999901388893/gestion-comptes.png?ex=6625c65f&is=6613515f&hm=c1d5921ad7fbad610f38c7df9a00cd36a008e2d5ab97f850df95a14d17b87ec2&'
			},
			{
				label: 'Page de connexion à l\'application mobile',
				src: 'https://cdn.discordapp.com/attachments/901792204224352266/1226715000576544768/app-connexion.png?ex=6625c65f&is=6613515f&hm=34810f9f1f1e2ca481d1695f8503c9408bb01ddf931c99fde9b7e7f91a8da01f&'
			},
		]
	},
	{
		slug: 'affinity',
		color: '#e21ace',
		description:
			'Affinity (nom temporaire) est une application mobile de rencontre sur Android et iOS. C\'est un projet personnel que je désire faire dans le but d\'améliorer mes compétences, mais surtout dans l\'espoir de gagner beaucoup d\'argent. L\'application en est encore au stade de la conception.',
		shortDescription:
			'Affinity (nom temporaire) est une application mobile de rencontre sur Android et iOS.',
		links: [],
		logo: Assets.Affinity,
		name: 'Affinity',
		period: {
			from: new Date(2024, 1, 1)
		},
		skills: getSkills('flutter', 'dart'),
		type: 'Application de rencontre',
		screenshots: []
	}
];

export const title = 'Projets';
