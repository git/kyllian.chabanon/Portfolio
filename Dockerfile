FROM node:latest AS build

WORKDIR /app

COPY package.json ./
RUN yarn install
COPY . ./
RUN npm run build

FROM nginx:1.19-alpine
COPY --from=build /app/build /usr/share/nginx/html
